import matplotlib.pyplot as plt
import numpy as np
import random

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('input_file', help='Txt input file (odometry and velocity)')
    parser.add_argument('--vel', action='store_true', help='Plot all graphics (including velocities)')
    parser.add_argument('--points', action='store_true', help='Draw some random points in the path')
    parser.add_argument('--direction', action='store_true', help='Draw an arrow indicating the direction of the path')
    args = parser.parse_args()

    data = np.loadtxt(args.input_file)
    points_ind = random.sample(range(data.shape[0]), 3)
    points_t = data[points_ind, 0]
    points_x = data[points_ind, 1]
    points_y = data[points_ind, 2]

    t = data[:, 0]
    x = data[:, 1]
    y = data[:, 2]
    theta = data[:, 3]
    lin_vel = data[:, 4]
    ang_vel = data[:, 5]

    fig1, ax1 = plt.subplots()
    ax1.set_title("Path")
    ax1.set_aspect('equal')
    ax1.set_xlabel("x (meters)")
    ax1.set_ylabel("y (meters)")
    if args.points:
        ax1.scatter(points_x, points_y, marker='x', c='red')
    if args.direction:
        arrow0 = x[1], y[1], x[1]-x[0], y[1]-y[0]
        head_width = (abs(y[1]-y[0])**2 + (abs(x[1]-x[0]))**2) * 5000
        ax1.arrow(*arrow0, shape='full', lw=0, length_includes_head=True, head_width=head_width, color='green') 
    ax1.plot(x, y)

    fig2, ax2 = plt.subplots()
    ax2.set_title("x(t)")
    ax2.set_xlabel("t (seconds)")
    ax2.set_ylabel("x (meters)")
    if args.points:
        ax2.scatter(points_t, points_x, marker='x', c='red')
    ax2.plot(t, x)

    fig3, ax3 = plt.subplots()
    ax3.set_title("y(t)")
    ax3.set_xlabel("t (seconds)")
    ax3.set_ylabel("y (meters)")
    if args.points:
        ax3.scatter(points_t, points_y, marker='x', c='red')
    ax3.plot(t, y)

    fig4, ax4 = plt.subplots()
    ax4.set_title("theta(t)")
    ax4.set_xlabel("t (seconds)")
    ax4.set_ylabel("theta (radians)")
    ax4.plot(t, theta)

    if args.vel:
        fig5, ax5 = plt.subplots()
        ax5.set_title("v(t)")
        ax5.set_xlabel("t (seconds)")
        ax5.set_ylabel("v (m/s)")
        ax5.plot(t, lin_vel)

        fig6, ax6 = plt.subplots()
        ax6.set_title("w(t)")
        ax6.set_xlabel("t (seconds)")
        ax6.set_ylabel("w (rad/s)")
        ax6.plot(t, ang_vel)
