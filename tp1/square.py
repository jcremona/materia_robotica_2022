import rospy
import math
from geometry_msgs.msg import Twist


if __name__ == '__main__':
    rospy.init_node('square')
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rate = rospy.Rate(10.0)

    while not rospy.is_shutdown():
        twist = Twist()
        twist.linear.x = 0.2
        twist.angular.z = 0
        
        # Go forward for 10 seconds (0.2 m/s)
        for i in range(100):
            pub.publish(twist)
            rate.sleep()

        # Stop 
        for i in range(10):
            twist = Twist()
            twist.linear.x = 0
            twist.angular.z = 0
            pub.publish(twist)
            rate.sleep()

        twist = Twist()
        twist.linear.x = 0
        twist.angular.z = math.pi / 4

        # Turn for 2 seconds (45 deg/s)
        for i in range(20):
            pub.publish(twist)
            rate.sleep()

        # Stop
        for i in range(10):
            twist = Twist()
            twist.linear.x = 0
            twist.angular.z = 0
            pub.publish(twist)
            rate.sleep()

